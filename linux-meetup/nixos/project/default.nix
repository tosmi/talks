let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;
  ruby = pkgs.ruby_2_2;
in stdenv.mkDerivation rec {
  name = "env";
  buildInputs = [
    ruby
    pkgs.bundler
  ];

  shellHook = ''
  export BUNDLE_PATH=.bundle
  export PATH=.bundle/bin:.bundle/gems/bin:$PATH
  export GEM_PATH=$PATH:.bundle/gems
  export GEM_HOME=.bundle/gems
  gem install bundler --no-ri --no-rdoc -v 1.14.4
  '';
}
